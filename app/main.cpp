#include "database.h"

using namespace emumba::training;

int main()
{
    database data;
    if (data.initialize_data_base())
    {

        data.poll_queries();
        return 0;
    }
    return -1;
}