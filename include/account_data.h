#ifndef _ACCOUNT_DATA_H
#define _ACCOUNT_DATA_H
#include <string>

namespace emumba
{
    namespace training
    {
        struct account_data_struct
        {
            std::string name;
            std::string bank;
            float balance;
            std::string currency;
        };

        class account_data
        {
        public:
            void set_name(const std::string &name);
            void set_bank(const std::string &bank);
            void set_balance(const float &balance);
            void set_currency(const std::string &currency);
            std::string get_name() const;
            std::string get_bank() const;
            float get_balance() const;
            std::string get_currency() const;

        private:
            account_data_struct data;
        };
    }
}

#endif