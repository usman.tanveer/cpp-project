#ifndef _DATABASE_H
#define _DATABASE_H
#include "account_data.h"
#include "personal_data.h"
#include "nlohmann/json.hpp"
#include <fstream>
#include <map>
#include <list>
#include <string>

namespace emumba
{
    namespace training
    {
        class database
        {
        public:
            bool read_json_file();
            bool initialize_data_base();
            void poll_queries();
            void query_response(const std::string &key, const std::string &value);
            void find_name(const std::string &value);
            void find_account_id(const std::string &value);
            void find_city(const std::string &value);
            void find_bank(const std::string &value);
            void find_currency(const std::string &value);
            void find_age(const int &value);
            void find_balance(const float &value);
            void find_latitude(const float &value);
            void find_longitude(const float &value);
            bool check_if_number(const std::string &value);

        private:
            std::map<std::string /*person's name*/, personal_data *> personal_info_map;
            std::list<personal_data> personal_info_list;
            std::map<std::string /*account id*/, account_data *> account_info_map;
            std::list<account_data> account_info_list;
        };
    }
}

#endif