#ifndef _PERSONAL_DATA_H
#define _PERSONAL_DATA_H
#include <string>

namespace emumba
{
    namespace training
    {
        struct Coordinates
        {
            float lat;
            float lon;
        };
        struct personal_data_struct
        {
            int age;
            std::string city;
            Coordinates coordinates;
        };

        class personal_data
        {
        public:
            void set_age(const int &age);
            void set_city(const std::string &city);
            void set_coordinates(const float &lat, const float &lon);
            int get_age() const;
            std::string get_city() const;
            Coordinates get_coordinates() const;

        private:
            personal_data_struct data;
        };
    }
}

#endif