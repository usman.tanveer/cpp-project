#include "account_data.h"

void emumba::training::account_data::set_name(const std::string &name)
{
    data.name = name;
}
void emumba::training::account_data::set_bank(const std::string &bank)
{
    data.bank = bank;
}
void emumba::training::account_data::set_balance(const float &balance)
{
    data.balance = balance;
}
void emumba::training::account_data::set_currency(const std::string &currency)
{
    data.currency = currency;
}
std::string emumba::training::account_data::get_name() const
{
    return data.name;
}
std::string emumba::training::account_data::get_bank() const
{
    return data.bank;
}
float emumba::training::account_data::get_balance() const
{
    return data.balance;
}
std::string emumba::training::account_data::get_currency() const
{
    return data.currency;
}