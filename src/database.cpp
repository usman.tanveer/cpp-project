#include "database.h"
#include <iomanip>
#include <unistd.h>

using json = nlohmann::ordered_json;

bool emumba::training::database::initialize_data_base()
{
    if (read_json_file())
    {
        return true;
    }
    return false;
}

void emumba::training::database::poll_queries()
{
    std::string str, key, value;
    std::ifstream myfile("../query.txt");
    if (!myfile.is_open())
    {
        std::ofstream outfile("../query_result.json");
        outfile << "{\"result\" : \"'query.txt' file is deleted of corrupted. closing program...!\"}";
        outfile.close();
        exit(-1);
    }

    while (true)
    {
        str.clear();
        myfile.seekg(0); //read from the start of file

        std::getline(myfile, str);
        if (str.empty())
        {
            std::ofstream outfile("../query_result.json");
            outfile << "{\"result\" : \"No query, closing program...!\"}";
            outfile.close();
            exit(-1);
        }
        key = str.substr(0, str.find(':'));
        value = str.substr(str.find(':') + 1, str.length());
        key = key.substr(key.find('"') + 1, key.rfind('"') - 1);
        value = value.substr(value.find('"') + 1, value.rfind('"') - 2);
        query_response(key, value);
        sleep(1);
    }
    myfile.close();
}

void emumba::training::database::find_name(const std::string &value)
{
    auto iter = personal_info_map.find(value);
    if (iter != personal_info_map.end())
    {
        json j = json::object(
            {{"name", iter->first},
             {"age", iter->second->get_age()},
             {"city", iter->second->get_city()},
             {"coordinates", {{"lat", iter->second->get_coordinates().lat}, {"long", iter->second->get_coordinates().lon}}}});
        std::ofstream outfile("../query_result.json");
        outfile << std::setw(4) << j;
        outfile.close();
    }
    else
    {
        std::ofstream outfile("../query_result.json");
        outfile << "{\"result\" : \"Name doesn't exist...!\"}";
        outfile.close();
    }
}

void emumba::training::database::find_account_id(const std::string &value)
{
    auto iter = account_info_map.find(value);
    if (iter != account_info_map.end())
    {
        json j = json::object(
            {{"name", iter->second->get_name()},
             {"bank", iter->second->get_bank()},
             {"account id", iter->first},
             {"balance", iter->second->get_balance()},
             {"currency", iter->second->get_currency()}});
        std::ofstream outfile("../query_result.json");
        outfile << std::setw(4) << j;
        outfile.close();
    }
    else
    {
        std::ofstream outfile("../query_result.json");
        outfile << "{\"result\" : \"Accound-id doesn't exist...!\"}";
        outfile.close();
    }
}

void emumba::training::database::find_city(const std::string &value)
{
    json j;
    for (auto iter : personal_info_map)
    {
        if (iter.second->get_city() == value)
        {
            j.push_back(
                {{"name", iter.first},
                 {"age", iter.second->get_age()},
                 {"city", iter.second->get_city()},
                 {"coordinates", {{"lat", iter.second->get_coordinates().lat}, {"long", iter.second->get_coordinates().lon}}}});
        }
    }
    std::ofstream outfile("../query_result.json");
    if (j.empty())
    {
        outfile << "{\"result\" : \"City doesn't exist...!\"}";
        outfile.close();
        return;
    }
    outfile << std::setw(4) << j;
    outfile.close();
}

void emumba::training::database::find_bank(const std::string &value)
{
    json j;
    for (auto iter : account_info_map)
    {
        if (iter.second->get_bank() == value)
        {
            j.push_back(
                {{"name", iter.second->get_name()},
                 {"bank", iter.second->get_bank()},
                 {"account id", iter.first},
                 {"balance", iter.second->get_balance()},
                 {"currency", iter.second->get_currency()}});
        }
    }
    std::ofstream outfile("../query_result.json");
    if (j.empty())
    {
        outfile << "{\"result\" : \"City doesn't exist...!\"}";
        outfile.close();
        return;
    }
    outfile << std::setw(4) << j;
    outfile.close();
}

void emumba::training::database::find_currency(const std::string &value)
{
    json j;
    for (auto iter : account_info_map)
    {
        if (iter.second->get_currency() == value)
        {
            j.push_back(
                {{"name", iter.second->get_name()},
                 {"bank", iter.second->get_bank()},
                 {"account id", iter.first},
                 {"balance", iter.second->get_balance()},
                 {"currency", iter.second->get_currency()}});
        }
    }
    std::ofstream outfile("../query_result.json");
    if (j.empty())
    {
        outfile << "{\"result\" : \"Currency doesn't exist...!\"}";
        outfile.close();
        return;
    }
    outfile << std::setw(4) << j;
    outfile.close();
}

void emumba::training::database::find_age(const int &value)
{
    json j;
    for (auto iter : personal_info_map)
    {
        if (iter.second->get_age() == value)
        {
            j.push_back(
                {{"name", iter.first},
                 {"age", iter.second->get_age()},
                 {"city", iter.second->get_city()},
                 {"coordinates", {{"lat", iter.second->get_coordinates().lat}, {"long", iter.second->get_coordinates().lon}}}});
        }
    }
    std::ofstream outfile("../query_result.json");
    if (j.empty())
    {
        outfile << "{\"result\" : \"Age doesn't exist...!\"}";
        outfile.close();
        return;
    }
    outfile << std::setw(4) << j;
    outfile.close();
}

void emumba::training::database::find_balance(const float &value)
{
    json j;
    for (auto iter : account_info_map)
    {
        if (iter.second->get_balance() == value)
        {
            j.push_back(
                {{"name", iter.second->get_name()},
                 {"bank", iter.second->get_bank()},
                 {"account id", iter.first},
                 {"balance", iter.second->get_balance()},
                 {"currency", iter.second->get_currency()}});
        }
    }
    std::ofstream outfile("../query_result.json");
    if (j.empty())
    {
        outfile << "{\"result\" : \"Balance doesn't exist...!\"}";
        outfile.close();
        return;
    }
    outfile << std::setw(4) << j;
    outfile.close();
}

void emumba::training::database::find_longitude(const float &value)
{
    json j;
    for (auto iter : personal_info_map)
    {
        if (iter.second->get_coordinates().lon == value)
        {
            j.push_back(
                {{"name", iter.first},
                 {"age", iter.second->get_age()},
                 {"city", iter.second->get_city()},
                 {"coordinates", {{"lat", iter.second->get_coordinates().lat}, {"long", iter.second->get_coordinates().lon}}}});
        }
    }
    std::ofstream outfile("../query_result.json");
    if (j.empty())
    {
        outfile << "{\"result\" : \"Longitude doesn't exist...!\"}";
        outfile.close();
        return;
    }
    outfile << std::setw(4) << j;
    outfile.close();
}

void emumba::training::database::find_latitude(const float &value)
{
    json j;
    for (auto iter : personal_info_map)
    {
        if (iter.second->get_coordinates().lat == value)
        {
            j.push_back(
                {{"name", iter.first},
                 {"age", iter.second->get_age()},
                 {"city", iter.second->get_city()},
                 {"coordinates", {{"lat", iter.second->get_coordinates().lat}, {"long", iter.second->get_coordinates().lon}}}});
        }
    }
    std::ofstream outfile("../query_result.json");
    if (j.empty())
    {
        outfile << "{\"result\" : \"Latitude doesn't exist...!\"}";
        outfile.close();
        return;
    }
    outfile << std::setw(4) << j;
    outfile.close();
}

bool emumba::training::database::check_if_number(const std::string &value)
{
    if (value.empty())
    {
        std::ofstream outfile("../query_result.json");
        outfile << "{\"result\" : \"Invalid value...\"}";
        outfile.close();
        return false;
    }
    for (auto c : value)
    {
        if (!isdigit(c))
        {
            if (!(c == '.'))
            {
                std::ofstream outfile("../query_result.json");
                outfile << "{\"result\" : \"Invalid value...\"}";
                outfile.close();
                return false;
            }
        }
    }
    return true;
}

void emumba::training::database::query_response(const std::string &key, const std::string &value)
{
    if (key == "name")
    {
        find_name(value);
    }
    else if (key == "age")
    {
        if (check_if_number(value))
        {
            find_age(stoi(value));
        }
    }
    else if (key == "city")
    {
        find_city(value);
    }
    else if (key == "bank")
    {
        find_bank(value);
    }
    else if (key == "account id")
    {
        find_account_id(value);
    }
    else if (key == "balance")
    {
        if (check_if_number(value))
        {
            find_balance(stof(value));
        }
    }
    else if (key == "currency")
    {
        find_currency(value);
    }
    else if (key == "latitude")
    {
        if (check_if_number(value))
        {
            find_latitude(stof(value));
        }
    }
    else if (key == "longitude")
    {
        if (check_if_number(value))
        {
            find_longitude(stof(value));
        }
    }
    else if (key == "clear")
    {
        if (value == "file")
        {
            std::ofstream outfile("../query_result.json");
            outfile << "";
            outfile.close();
        }
    }
    else
    {
        std::ofstream outfile("../query_result.json");
        outfile << "{\"result\" : \"Invalid query, try another...\"}";
        outfile.close();
    }
}

bool emumba::training::database::read_json_file()
{
    std::ifstream infile("../personal_data.json");
    if (!infile.is_open())
    {

        std::ofstream outfile("../query_result.json");
        outfile << "{\"result\" : \"'personal_data.json' file is deleted of corrupted. closing program...!\"}";
        outfile.close();
        return false;
    }
    json data;
    try
    {
        data = json::parse(infile); //check if 'personal_data.json' is valid json file
    }
    catch (const std::exception &e)
    {
        std::ofstream outfile("../query_result.json");
        outfile << "{\"result\" : \"'personal_data.json' file is not valid. closing program...!\"}";
        outfile.close();
        exit(-1);
    }
    personal_data person_obj;
    account_data account_obj;
    for (auto person : data["Account Holders Info"])
    {
        person_obj.set_age(person["age"]);
        person_obj.set_city(person["city"]);
        person_obj.set_coordinates(person["coordinates"]["lat"], person["coordinates"]["long"]);

        personal_info_list.push_back(person_obj);
        personal_info_map[person["name"]] = &personal_info_list.back();

        account_obj.set_name(person["name"]);
        for (auto account : person["accounts"])
        {
            account_obj.set_bank(account["bank"]);
            account_obj.set_balance(account["balance"]);
            account_obj.set_currency(account["currency"]);

            account_info_list.push_back(account_obj);
            account_info_map[account["account id"]] = &account_info_list.back();
        }
    }
    infile.close();
    return true;
}