#include "personal_data.h"

void emumba::training::personal_data::set_age(const int &age)
{
    data.age = age;
}
void emumba::training::personal_data::set_city(const std::string &city)
{
    data.city = city;
}
void emumba::training::personal_data::set_coordinates(const float &lat, const float &lon)
{
    data.coordinates.lat = lat;
    data.coordinates.lon = lon;
}
int emumba::training::personal_data::get_age() const
{
    return data.age;
}
std::string emumba::training::personal_data::get_city() const
{
    return data.city;
}
emumba::training::Coordinates emumba::training::personal_data::get_coordinates() const
{
    return data.coordinates;
}